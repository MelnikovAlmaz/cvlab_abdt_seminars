# Object Detection demo

## Getting started / Подготовка проекта
1. cd <path_to_demo>/yolo-coco
2. wget https://pjreddie.com/media/files/yolov3.weights
3. cd ..
4. virtualenv --no-site-packages --python=python3 venv
5. source venv/bin/activate
6. pip install -r requirements.txt
7. python3 yolo_video.py

## Issues / Неполадки
1. If you will have errors with video camera, change video camera id by adding argument --web-camera <camera_id>

