import argparse
import time

import numpy as np
import cv2
import imutils

# initialize the video stream, then allow the camera sensor to warm up
from imutils.video import VideoStream

ap = argparse.ArgumentParser()
ap.add_argument("-w", "--web-camera", type=int, default=0,
	help="path to input video")
args = vars(ap.parse_args())

print("[INFO] starting video stream...")
vs = VideoStream(src=args['web_camera']).start()
time.sleep(2.0)

method = 'canny'
is_car_number_find = False
background = None

# loop over frames from the video file stream
while True:
    # grab the frame from the threaded video stream
    frame = vs.read()
    # resize the frame to have a width of 600 pixels (while
    # maintaining the aspect ratio), and then grab the image
    # dimensions

    # RGB to Gray scale conversion
    gray = cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2GRAY)

    # Noise removal with iterative bilateral filter(removes noise while preserving edges)
    gray = cv2.bilateralFilter(gray, 11, 17, 17)

    if method == 'canny':
        # Find Edges of the grayscale image
        output_img = cv2.Canny(gray, 100, 150)
    if method == 'laplacian':
        output_img = cv2.Laplacian(gray, cv2.CV_64F)
    if method == 'oriented_grad':
        # Calculate gradients
        image = np.float32(frame.copy()) / 255.0
        gx = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=1)
        gy = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=1)
        mag, angle = cv2.cartToPolar(gx, gy, angleInDegrees=True)
        output_img = mag
    if method == 'find_number':
        edged = cv2.Canny(gray, 100, 150)
        # Find contours based on Edges
        cnts, hierarchy = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # Sort contours based on their area keeping minimum required area as '30'
        # (anything smaller than this will not be considered)
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:30]
        NumberPlateCnt = None  # we currently have no Number plate contour

        # Loop over our contours to find the best possible approximate contour of number plate
        count = 0
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)
            # Select the contour with 4 corners
            if len(approx) == 4:
                # This is our wapprox Number Plate Contour
                NumberPlateCnt = approx
                break

        # Drawing the selected contour on the original image
        output_img = frame.copy()
        try:
            cv2.drawContours(output_img, [NumberPlateCnt], -1, (0, 255, 0), 3)
        except Exception as e:
            pass
    if method == 'color_segmentation':
        # converting from BGR to HSV color space
        hsv = cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2HSV)

        # Range for lower red
        lower_red = np.array([0, 120, 70])
        upper_red = np.array([10, 255, 255])
        mask1 = cv2.inRange(hsv, lower_red, upper_red)

        # Range for upper range
        lower_red = np.array([170, 120, 70])
        upper_red = np.array([180, 255, 255])
        mask2 = cv2.inRange(hsv, lower_red, upper_red)

        mask = mask1 + mask2

        mask1 = cv2.morphologyEx(mask, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8))
        mask1 = cv2.morphologyEx(mask, cv2.MORPH_DILATE, np.ones((3, 3), np.uint8))

        # creating an inverted mask to segment out the cloth from the frame
        mask2 = cv2.bitwise_not(mask1)

        # Segmenting the cloth out of the frame using bitwise and with the inverted mask
        res1 = cv2.bitwise_and(frame.copy(), frame.copy(), mask=mask2)

        # creating image showing static background frame pixels only for the masked region
        res2 = cv2.bitwise_and(background, background, mask=mask1)

        # Generating the final output
        final_output = cv2.addWeighted(res1, 1, res2, 1, 0)

        output_img = final_output

    output_img = imutils.resize(output_img, output_img.shape[0] * 2)
    cv2.imshow("Frame", output_img)
    key = cv2.waitKey(1) & 0xFF
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break
    if key == ord("w"):
        if method == 'canny':
            method = 'laplacian'
        elif method == 'laplacian':
            method = 'find_number'
        elif method == 'find_number':
            method = 'oriented_grad'
        elif method == 'oriented_grad':
            method = 'color_segmentation'
            background = frame
        elif method == 'color_segmentation':
            method = 'canny'

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()

