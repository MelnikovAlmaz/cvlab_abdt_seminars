# import the necessary packages
from tensorflow.keras.preprocessing import image as image_utils
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
from tensorflow.keras.applications.resnet50 import ResNet50
import numpy as np
import argparse
import cv2
import time
import imutils
from imutils.video import VideoStream

ap = argparse.ArgumentParser()
ap.add_argument("-w", "--web-camera", type=int, default=0,
	help="path to input video")
args = vars(ap.parse_args())

# load the VGG16 network pre-trained on the ImageNet dataset
print("[INFO] loading network...")
model = ResNet50(weights='imagenet')
model_input_size = 224

# initialize the video stream, then allow the camera sensor to warm up
print("[INFO] starting video stream...")
vs = VideoStream(src=args['web_camera']).start()
time.sleep(2.0)
frame_index = 0

# loop over frames from the video file stream
while True:
    # grab the frame from the threaded video stream
    frame = vs.read()
    frame_index = frame_index + 1
    image = frame.copy()
    image = imutils.resize(image, height=model_input_size)
    width_offset = (image.shape[1] - model_input_size) // 2
    image = image[:, width_offset:width_offset + model_input_size, :]
    image = image_utils.img_to_array(image)
    
    image = np.expand_dims(image, axis=0)
    image = preprocess_input(image)
    
    # classify the image
    preds = model.predict(image)
    P = decode_predictions(preds)
    # loop over the predictions and display the rank-5 predictions +
    # probabilities to our terminal
    frame = imutils.resize(frame, int(frame.shape[0] * 2.5))
    for (i, (imagenetID, label, prob)) in enumerate(P[0]):
        print("{}. {}: {:.2f}%".format(i + 1, label, prob * 100))
        cv2.rectangle(img=frame, pt1=(10, 30*(i+1)), pt2=(350, 30*(i)+5), color=[0, 0, 0], thickness=-1)
        cv2.putText(frame, "{}, {:.2f}%".format(label, prob * 100), (10, 30*(i+1)), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 1)
    if frame_index % 3 == 0:
        cv2.imshow("Frame", frame)
    
    key = cv2.waitKey(1) & 0xFF
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()


